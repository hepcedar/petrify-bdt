# SKL BDT
# TMVA BDT
# TMVA MLP
Convertor that takes a TMVA MLP (as an XML file) and produces an ONNX file using Keras.
Usage:
```bash
python3 tmvamlp.py <weights file>.xml
```
Use `--help` to see further options.
